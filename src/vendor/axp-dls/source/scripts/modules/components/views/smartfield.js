//
// SmartField Component
//

import View from '../../core/view';
import Component from '../classes/component';
import SmartFieldModel from '../models/smartfield.js';
import SmartFieldTemplate from './templates/tmpl-smartfield.html!text';

//
// Configuration
//
const Name = 'SmartField';
const Selector = '[data-toggle="smartfield"]:not([data-ignore])';
const DataKey = 'dls.smartfield';
const EventKey = `.${DataKey}`;

//
// Initiator
//
export default class SmartField extends Component {

	constructor (opts) {
		super(opts);
		this.component = SmartFieldView;

		this.render();
	}
}

//
// SmartField View
//
class SmartFieldView extends View {

	constructor (opts) {
		super(opts);

		this.configure();
		this.render();

		this.setProps();
		this.updateValidation(true);
		this.defaultState();

		this.listen();
	}

	//
	// Getters
	//
	static get Name() {return Name;}
	static get Selector() {return Selector;}
	static get EventKey() {return EventKey;}

	//
	// Configure
	//
	configure() {
		let fields = [],
			legend = this.target.find('legend'),
			inputs = this.target.find('input');

		if(legend.length)
			this.model.set('legend', legend.html());

		inputs.each( (idx, el) => {
			el = $(el);

			let state = el.hasClass('has-warning') ? 'has-warning' : '',
				required = el.prop('required') ? 'required' : '',
				describedBy = el.attr('aria-describedby'),
				validation = this.target.find(`#${describedBy}`),
				name = el.attr('name') || el.attr('id'),
				label = this.target.find(`label[for="${name}"]`),
				field = {
					type: el.prop('type') || 'text',
					label: '&nbsp;',
					state: state,
					name: name,
					value: el.val(),
					required: required,
					validation: '',
					described: ''
				};

			if(label.length === 1)
				field.label = label.html();

			if(validation.length === 1) {
				field.described = `aria-describedby="${describedBy}"`;
				field.validation = validation[0].outerHTML;
			}

			fields.push(field);
		});

		this.model.set('fields', fields);
	}

	//
	// Render
	//
	render() {
		let field = $(this.util.request('template:render',
			SmartFieldTemplate,
			this.model.attributes
		));

		this.el = field.find('.smart-field');
		this.validation = field.find('.smart-field-validation');

		this.target
			.empty()
			.prepend(field.children());
	}

	//
	// Set Props
	//
	setProps() {
		this.sel = {
			placeholder: this.el.find('.smart-el-placeholder'),
			field: this.el.find('.smart-el-field'),
			fields: this.el.find('.smart-el-inputs'),
			input: this.el.find('.smart-el-input')
		};
	}

	//
	// Bind the events
	//
	listen() {
		this.el
			.on(this.platform.event.down + EventKey,
				$.proxy(this.onMouseDown, this)
			)
			.on(`click${EventKey}`, $.proxy(this.onFocus, this));

		this.sel.fields
			.find('input')
			.on(`change${EventKey}`, $.proxy(this.onChange, this))
			.on(`focus${EventKey}`, $.proxy(this.onFocus, this))
			.on(`blur${EventKey}`,
				this.util.request('events:throttle',
					$.proxy(this.onBlur, this),
					1
				)
			);

		if(this.model.get('forceFocus')) {
			this.onChange();
			this.onFocus();
		}
	}

	//
	// Field Pressed
	//
	onMouseDown(e) {
		this.pressed = true;

		$(document).on(this.platform.event.up + EventKey + this.uid,
			$.proxy(this.onMouseUp, this)
		);
	}

	//
	// Field Released
	//
	onMouseUp(e) {
		this.pressed = false;
		this.onBlur();
		$(document).off(this.platform.event.up + EventKey + this.uid,
			$.proxy(this.onMouseUp, this)
		);
	}

	//
	// Field changed
	//
	onChange() {
		this.changed = true;
	}

	//
	// Field Focus
	//
	onFocus() {
		if(this.target.is(':disabled') || this.state === 'active')
			return this;

		this.updateState('active');
	}

	//
	// Field Blur
	//
	onBlur() {
		if(this.sel.input.find('input:focus').length ||
			this.state === 'default' ||
			this.pressed)
				return this;

		this.updateState('default');
	}

	//
	// Update State
	//
	updateState(state) {
		if(state)
			this.state = state;

		if(this.state === 'active')
			this.activeState();
		else if(this.state === 'default')
			this.defaultState();
	}

	//
	// Field is in active (selected) state
	//
	activeState() {
		this.el
			.addClass('active focus');

		if(this.model.get('forceFocus') && !this.firstForce) {
			this.firstForce = true;
		} else {
			this.sel.input
				.first()
				.find('input')
				.trigger('focus');
		}
	}

	//
	// Field is in its default (unselected) state
	//
	defaultState() {
		if(!this.model.get('forceFocus'))
			this.el
				.removeClass('active focus');

		this.updateValue();
		this.validate();
		this.updateValidation();
	}

	//
	// Update the value
	//
	updateValue() {
		let value = '';

		this.sel.fields
			.find('input')
			.each( (idx, el) => {
				let val = $(el).val();

				if(val !== '')
					value += (value!=='' ? ' ' : '') +val;
			});

		this.sel.field
			.find('input')
			.val(value);

		if(value !== '')
			this.el.addClass('has-value');
		else
			this.el.removeClass('has-value');
	}

	//
	// Validate the field, if necessary
	//
	validate(force) {
		this.sel.input
			.each( (idx, el) => {
				el = $(el);

				let input = el.find('input'),
					valid = true,
					type = input.prop('type'),
					val = input.val();

				if(input.is(':required')) {
					switch(type) {
						case 'text':
							if(val.replace(/ /g, '') === '')
								valid = false;
							break;
						default: break;
					};
				}

				if(this.changed && !force) {
					let alertID = input.attr('aria-describedby'),
						alert = $(`#${alertID}`);

					if(!valid) {
						el.addClass('has-warning');
						if(alert.length)
							alert.addClass('active');
					} else {
						el.removeClass('has-warning');
						if(alert.length)
							alert.removeClass('active');
					}

				}
			});
	}

	//
	// Update validation
	//
	updateValidation(force) {
		if(!this.changed && !force)
			return;

		let warning = false,
			success = true;

		this.sel.input
			.each( (idx, el) => {
				el = $(el);

				let input = el.find('input'),
					alertID = input.attr('aria-describedby'),
					alert = $(`#${alertID}`);

				if(el.hasClass('has-warning')) {
					success = false;
					warning = true;

					if(alert.length)
						alert.addClass('active');
				} else if(input.is(':required') && input.val() === '') {
					success = false;
				}
			});

		if(warning)
			this.el
				.addClass('smart-field-warning');
		else
			this.el
				.removeClass('smart-field-warning');

		if(success && !warning)
			this.el.addClass('smart-field-success')
		else
			this.el.removeClass('smart-field-success');
	}

	//
	// Interface
	//
	static _interface(opts) {
		let $el = $(this),
			data = $el.data(DataKey),
			config = Object.assign(
				{},
				$el.data(),
				opts
			);

		if(!data) {
			data = new SmartFieldView({
				target: $el,
				model: new SmartFieldModel(config)
			});

			$el.data(DataKey, data);
		}

		return data;
	}

	//
	// Destroy
	//
	destroy() {
		$.removeData(this.target, DataKey);
	}
}
