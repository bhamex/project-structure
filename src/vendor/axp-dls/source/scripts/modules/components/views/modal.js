//
// Modal Component
//

import View from '../../core/view';
import Component from '../classes/component';
import ModalModel from '../models/modal';
import ModalTemplate from './templates/tmpl-modal.html!text';
import ModalControlBackTemplate from './templates/tmpl-modal-control-back.html!text';
import ModalControlCloseTemplate from './templates/tmpl-modal-control-close.html!text';

//
// Configuration
//
const Name = 'Modal';
const DataKey = 'dls.modal';
const EventKey = `.${DataKey}`;
const Config = {
	timeout: {
		show: null,
		hide: null
	},
	hideDelay: 200
}

//
// Initiator
//
export default class Modal extends Component {

	constructor (opts) {
		super(opts);
		this.component = ModalView;
	}

	//
	// Modal `Create` (differs from standard create in that it doesn't)
	// require a target to be specified
	//
	create(options) {
		if(!this.component)
			return;

		if(!options.target) {
			options.target = $('<div />');
		}

		let elements = [];
		options.config = Object.assign({}, options.config);
		options.target = $(options.target);
		options.target.each((idx, el) => {
			let element = this.component._interface.call($(el), options.config);

			if(element)
				elements.push(element);
		});

		return this.created(elements);
	}

}

//
// Modal View
//
class ModalView extends View {

	constructor (opts) {
		super(opts);
		this.setup();
		this.render();
		this.setProps();

		this.listen();
	}

	//
	// Getters
	//
	static get Name() {return Name;}
	static get EventKey() {return EventKey;}

	//
	// Setup
	//
	setup() {
		let {
			controlsLeft,
			controlsRight
		} = this.model.attributes;
		controlsLeft = this.generateControls(
			controlsLeft.replace(/ /g, '').split(',')
		);

		controlsRight = this.generateControls(
			controlsRight.replace(/ /g, '').split(',')
		);

		this.model.set('controlsLeft', controlsLeft);
		this.model.set('controlsRight', controlsRight);
	}

	//
	// Generate Controls
	//
	generateControls(list) {
		let str = '';
		list.forEach((item) => {
			switch(item) {
				case 'back':
					str += ModalControlBackTemplate;
					break;
				case 'close':
					str += ModalControlCloseTemplate;
					break;
				default: break;
			}
		});

		return str;
	}

	//
	// Render the component
	//
	render() {
		this.el = $( this.util.request(
			'template:render',
			ModalTemplate,
			this.model.attributes
		));
	}

	//
	// Set the properties
	//
	setProps() {
		this.sel = {
			title: this.el.find('.modal-title'),
			content: this.el.find('.modal-content'),
			header: this.el.find('.modal-header'),
			controlsLeft: this.el.find('.modal-controls-left'),
			controlsRight: this.el.find('.modal-controls-right')
		};
	}

	//
	// Bind the listeners
	//
	listen() {
		this.model
			.on('change', $.proxy(this.onChanged, this));

		this.app.vent
			.on(`component:${Name}:open`,
				$.proxy(this.onModalOpened, this)
			)
			.on(`component:${Name}:close`,
				$.proxy(this.onModalClosed, this)
			);
	}

	activeListen() {
		this.el
			.on(`click${EventKey}`, '.modal-control-back', () => {
				return this.close();
			});
	}

	//
	// Model changed
	//
	onChanged(obj) {

		if(obj.content != null)
			this.sel.content.html(obj.content);

		if(obj.title !== undefined)
			this.sel.title.html(obj.title);
	}

	//
	// On Modal Opened (triggered when ANY modal opens)
	//
	onModalOpened(uid) {
		if(uid === this.uid)
			return;

		if(this.model.get('open'))
			this.close();
	}

	//
	// On Modal Closed (triggered when ANY modal closes)
	//
	onModalClosed(uid) {
		if(uid === this.uid)
			return;
	}

	//
	// Proxy `open`
	//
	show(...args) {
		return this.open(...args);
	}

	//
	// Proxy `close`
	//
	hide(...args) {
		return this.close(...args);
	}

	//
	// Open modal
	//
	open() {
		let container = this.model.get('container');
		clearTimeout(Config.timeout.show);
		clearTimeout(Config.timeout.hide);

		this.el.removeClass('in');
		this.model.set('open', true);

		this.emit('opened');

		this.app.vent
			.trigger(`component:${Name}:open`, this.uid);

		container.append(this.el);

		Config.timeout.show = setTimeout(() => {
			this.el.addClass('in');
		}, 10);

		this.emit('opening');

		this.activeListen();

		return this;
	}

	//
	// Close modal
	//
	close() {
		this.model.set('open', false);
		clearTimeout(Config.timeout.show);
		clearTimeout(Config.timeout.hide);

		this.app.vent
			.trigger(`component:${Name}:close`, this.uid);

		this.el.removeClass('in');

		this.emit('closing');

		Config.timeout.hide = setTimeout(() => {
			this.emit('closed');
			this.el.remove();
		}, Config.hideDelay);

		return this;
	}

	//
	// Interface
	//
	static _interface(opts) {
		let $el = $(this),
			data = $el.data(DataKey),
			config = Object.assign(
				{},
				$el.data(),
				opts
			);

		if(!data) {
			data = new ModalView({
				target: $el,
				model: new ModalModel(config)
			});

			$el.data(DataKey, data);
		} else {
			console.warn(
				`DLS > ${Name}: A modal view is already attached to this element. ` +
				'Use the destroy() method for the existing modal.'
			);
		}

		return data;
	}

	//
	// Destroy
	//
	destroy() {
		$.removeData(this.target, DataKey);
	}
}
