//
// Templates Handler
//
// On-the-fly templating

import Obj from '../core/obj';

export default class Templates extends Obj {

	constructor (opts) {
		super(opts);

		this.escaper = /\\|'|\r|\n|\u2028|\u2029/g;
		this.listen();
	}

	//
	// Listen
	//
	listen() {
		this.util.reply('template:render', this.render, this);
	}

	//
	// Render
	//
 	render(html, opts) {
		opts = opts || {};

		let template = html,
			data = opts || {};

		//Must have template passed to render
		if (!template)
			return;

		//If No Opts fallback to defaults
		var settings = this.settings,
			escaper = this.escaper,
			escapes = this.escapes,
			escapeChar = (match) => {
				return '\\' + escapes[match];
			};

		//Compile Vars & Fn
		var index = 0,
			source = "__p+='",
			matcher = RegExp([
				(settings.escape || noMatch).source,
				(settings.interpolate || noMatch).source,
				(settings.evaluate || noMatch).source
			].join('|') + '|$', 'g');

		// Compile the template source, escaping string literals appropriately.
		template
			.replace(matcher, function(match, escape, interpolate, evaluate, offset) {
				source += template.slice(index, offset).replace(escaper, escapeChar);
				index = offset + match.length;

				if (escape)
					source += "'+\n((__t=(" + escape + "))==null?'':_.escape(__t))+\n'";
				else if (interpolate)
					source += "'+\n((__t=(" + interpolate + "))==null?'':__t)+\n'";
				else if (evaluate)
					source += "';\n" + evaluate + "\n__p+='";

				// Adobe VMs need the match returned to produce the correct offest.
				return match;
			});

		source += "';\n";

		settings.variable = 'obj';
		source = 'with(obj||{}){\n' + source + '}\n';

		source = "var __t,__p='',__j=Array.prototype.join," +
		"print=function(){__p+=__j.call(arguments,'');};\n" +
		source + 'return __p;\n';

		var _this = this;

		this.source = 'function(' + settings.variable + '){\n' + source + '}';

		let output = new Function(settings.variable, '_', source).call(this, data, _this);
		output = output
			.trim()
			.replace(/\r/g, '')
			.replace(/\n/g, '')
			.replace(/\s+/g, ' ')
			.replace(/(">\s)/g, '">')
			.replace(/('>\s)/g, "'>")
			.replace(/(\s<)/g, '<')

		return output;
	}
}

Templates.prototype.settings = {
	evaluate: /<%([\s\S]+?)%>/g,
	interpolate: /<%=([\s\S]+?)%>/g,
	escape: /<%-([\s\S]+?)%>/g
};

Templates.prototype.escapes = {
	"'": "'",
	'\\': '\\',
	'\r': 'r',
	'\n': 'n',
	'\u2028': 'u2028',
	'\u2029': 'u2029'
};
