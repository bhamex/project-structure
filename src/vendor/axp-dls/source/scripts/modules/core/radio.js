//
// Requests
//

import Obj from './obj';

const EventSplitter = /\s+/;
const Logs = {};
let uid = 0;

let removeHandler = (store, name, callback, context) => {
	let event = store[name];
	if(
		(!callback || (callback === event.callback || callback === event.callback._callback)) &&
		(!context || (context === event.context))
	) {
		delete store[name];
		return true;
	}
},
removeHandlers = (store, name, callback, context) => {
	store || (store = {});
	let names = name ? [name] : Object.keys(store),
		matched = false;

	for(var i = 0, il = names.length; i < il; ++i) {
		names = names[i];

		if(!store[name])
			continue;

		if(removeHandler(store, name, callback, context)) {
			matched = true;
		}
	}

	return matched;
},
getUID = () => {
	return uid++;
},
makeCallback = (callback) => {
  return typeof callback === 'function' ? callback : () => { return callback; };
};


//
// Radio
//
class Radio extends Obj {

	constructor (opts) {
		super(opts);
	}

	//
	// Perform request action
	//
	request(name, ...args) {
		let results = Radio._eventsApi(this, 'request', name, args);

		if(results)
			return results;

		let channelName = this.channelName,
			requests = this._requests;

		if(requests && (requests[name] || requests['default'])) {
			let handler = requests[name] || requests['default'];
			args = requests[name] ? args: arguments;
			return Radio._callHandler(handler.callback, handler.context, args);
		} else {
			//console.log([`An unhandled request was fired: ${name}`, name, channelName]);
		}
	}

	//
	// Apply a reply handler
	//
	reply(name, callback, context) {
		if(Radio._eventsApi(this, 'reply', name, [callback, context]))
			return this;

		this._requests || (this._requests = {});

		if(this._requests[name]) {
			//console.log([`A request was overwritten: ${name}`, name, this.channelName]);
		}

		this._requests[name] = {
			callback: makeCallback(callback),
			context: context || this
		};

		return this;
	}

	//
	// Apply a one-time reply handler (subsequent requests will not be called)
	//
	replyOnce(name, callback, context) {
		if(Radio._eventsApi(this, 'replyOnce', name, [callback, context])) {
			return this;
		}

		let once = function (...args) {
			this.stopReplying(name);
			return makeCallback(callback).apply(this, ...args);
		}

		return this.reply(name, once, context);
	}

	//
	// Stop replying
	//
	stopReplying(name, callback, context) {
		if(Radio._eventsApi(this, 'stopReplying', name))
			return this;

		if(!name && !callback && !context) {
			delete this._requests;
		} else if(!removeHandlers(this._requests, name, callback, context)) {
			//console.log([`Attempted to remove the unregistered request`, name, this.channelName]);
		}

		return this;
	}

	//
	// Create a sub channel
	//
	channel(channelName) {
		if(!channelName)
			throw new Error(`You must provide a name for the channel.`);

		if(this._channels[channelName])
			return this._channels[channelName];
		else
			return this._channels[channelName] = new Channel(channelName);
	}

	//
	// Static API: Events
	//
	static _eventsApi(obj, action, name, rest) {
		if(!name)
			return false;

		let results = {};

		if(typeof name === 'object') {
			for(let key in name) {
				let result = obj[action].apply(obj, [key, name[key]].concat(rest));
				EventSplitter.test(key) ? Object.assign(results, result) : results[key] = result;
			}

			return results;
		}

		if(EventSplitter.test(name)) {
			let names = name.split(EventSplitter);
			for(var i = 0, il = names.length; i < il; ++i)
				results[names[i]] = obj[action].apply(obj, [names[i]].concat(rest));
			return results;
		}

		return false;
	}

	//
	// Static API: Call handling
	//
	static _callHandler(callback, context, args) {
		var a1 = args[0],
			a2 = args[1],
			a3 = args[2];

		switch(args.length) {
			case 0: return callback.call(context);
			case 1: return callback.call(context, a1);
			case 2: return callback.call(context, a1, a2);
			case 3: return callback.call(context, a1, a2, a3);
			default: return callback.apply(context, args);
		}
	}
}

//
// Channel Creator
//
export default class Channel extends Radio {

	constructor (opts) {
		super(opts);
		this.channelName = this.channelName || 'channel-'+getUID();
	}

	reset() {
		this.off();
		this.stopReplying();
		return this;
	}
}
