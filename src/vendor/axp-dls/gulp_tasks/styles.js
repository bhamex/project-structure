//
// Styles
//
// Generate the stylesheets (The uncompressed version includes source maps)

'use-strict';

module.exports = function(gulp, config, $) {

	var isRelease = config.argv.release;

	gulp.task('styles', function(cb) {

		return gulp.src(config.styles.src)
			.pipe($.sassBulkImport())

			// Export with sourcemaps
			.pipe($.sourcemaps.init())
			.pipe($.sass({
				outputStyle: 'nested',
				precision: 10
			}))
			.on('error', $.notify.onError({
				title: "DLS Styles Error",
				message: 'Error compiling DLS styles.',
				sound: false
			}))
			.on('error', function(error) {
				console.log(error);
				this.emit('end');
			})
			.pipe($.save('before-sourcemaps'))
			.pipe($.sourcemaps.write('.'))
			.pipe(gulp.dest(config.styles.dest))
			.pipe($.save.restore('before-sourcemaps'))
			// Minify and export
			.pipe($.autoprefixer({
				browsers: ['last 2 versions', '> 1%', 'ie 8']
			}))
			.pipe($.mergeMediaQueries({log: false}))
			.pipe(gulp.dest(config.styles.dest))
			.pipe($.cssnano({zindex: false}))
			.pipe($.rename('dls.min.css'))
			.pipe(gulp.dest(config.styles.dest));
	});
};
