var CryptoJS = require("crypto-js");
export class AmexSecurityService {
  KEY_ENCRYPTION = "KEY_ENCRYPTION_PLACEHOLDER";
  public doEncrypt(val){
      console.info('set method val != empty');
      var ciphertext = CryptoJS.AES.encrypt(val, this.KEY_ENCRYPTION);
      console.log('encrypt = ' + ciphertext.toString());
      return ciphertext.toString();
  }
  public doDecrypt(val){
    var bytes  = CryptoJS.AES.decrypt(val, this.KEY_ENCRYPTION);
    var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    console.log('decrypt = ' + JSON.stringify(decryptedData));
    return JSON.stringify(decryptedData);
  }
}
