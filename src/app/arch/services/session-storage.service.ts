import { Injectable } from '@angular/core';
import { AmexSecurityService } from './amex-security.service';

@Injectable()
export class SessionStorageService {
  constructor (public securityLayer: AmexSecurityService){  }
  public encryptedValue:any;
  public decryptedValue:any;

  public setSessionObject(input){
    console.info('set method call' + input);
    if(input !== '' || input !== null || input.trim() !== ''){
      this.encryptedValue = this.securityLayer.doEncrypt(input);
      this.decryptedValue = this.securityLayer.doDecrypt(this.encryptedValue);
      localStorage.setItem('keyVal',this.encryptedValue);
    }
  }

}
