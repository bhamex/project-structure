import { Component, OnInit, Input, HostListener } from '@angular/core';

@Component({
  selector: 'app-amex-navigation-util',
  templateUrl: './amex-navigation-util.component.html'
  //styleUrls: ['./amex-navigation-util.component.less']
})
export class AmexNavigationUtilComponent {
  @Input() public navigation = () => {};

  ngOnInit() {

  }

  @HostListener ('click',['$event'])
    callbackInt(){
      this.navigation();
  }

}
