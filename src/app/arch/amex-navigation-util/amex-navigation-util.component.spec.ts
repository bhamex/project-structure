/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { AmexNavigationUtilComponent } from './amex-navigation-util.component';

describe('AmexNavigationUtilComponent', () => {
  let component: AmexNavigationUtilComponent;
  let fixture: ComponentFixture<AmexNavigationUtilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmexNavigationUtilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmexNavigationUtilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
