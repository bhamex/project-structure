import { Component, OnInit, Input, HostListener } from '@angular/core';

@Component({
  selector: 'amex-input',
  templateUrl: './amex-input.component.html',
  styleUrls: ['./amex-input.component.css']
})
export class AmexInputComponent implements OnInit {
  @Input() public inputFormat;
  @Input() public bindVariable;
  @Input() public inputPlaceholder;
  @Input() public inputLable;
  @Input() public inputIcon;
  @Input() public maxLength;
  @Input() public minLength;
  @Input() public limitChar;
  @Input() public isRequired = false;
  @Input() public isDisabled = false;
  @Input() public onFocus;
  @Input() public onBlur;
  //@Input() public onChange = () => {};
  @Input() public actionBtnIcon = () => {};

  ngOnInit() {
    if(this.inputFormat ===""){
      this.inputFormat="text";
    }
  }

  //public onChangeModel(val){
    //this.onChange(val);
  //}

  /*ngOnChanges(changes: {[this.bindVariable: string]: SimpleChange}) {
    if (changes['fontSize']) { // fire your event }
  }

  @HostListener ('onchange',['$event'])
    onChangeInt(){
      this.onChange();
  }*/

}
