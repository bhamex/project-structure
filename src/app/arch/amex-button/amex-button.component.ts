import { Component, Input, HostListener, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'amex-button',
  templateUrl: './amex-button.component.html',
  styleUrls: ['./amex-button.component.less']
})
export class AmexButtonComponent {
  //parameter
  @Input() btnLable = '';
  @Input() btnType;
  @Input() btnIcon;
  @Input() btnOnlyIcon;
  @Input() btnDisable=false;
  //page load variable
  btnText="go to google";
  btnClass="";

  @Output() callback: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    if(this.btnType === "simple"){
      this.btnClass="btn-full-width btn-primary";
      this.btnIcon="";
    }else if(this.btnType === "simple-icon"){
      this.btnClass="btn-icon";
    }else if(this.btnOnlyIcon ==="icon"){
      this.btnLable="";
    }
  }
  onClick(e) {
    this.callback.emit(e);
  }

}
