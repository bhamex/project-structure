import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { routing } from './app.routing';

import { AppComponent } from './app.component';

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { EssentialsComponent } from './essentials/essentials.component';
import { FaqComponent } from './faq/faq.component';
import { OnlinePrivacyComponent } from './online-privacy/online-privacy.component';
import { PartnerProductApiComponent } from './product/partner-product-api/partner-product-api.component';
import { ProductsApiComponent } from './product/products-api/products-api.component';
import { AmexInputComponent } from './arch/amex-input/amex-input.component';
import { AmexHeaderComponent } from './arch/amex-header/amex-header.component';
import { AmexFooterComponent } from './arch/amex-footer/amex-footer.component';
import { AmexButtonComponent } from './arch/amex-button/amex-button.component';
import { AmexNavigationUtilComponent } from './arch/amex-navigation-util/amex-navigation-util.component';
import { CreateAppComponent } from './create-app/create-app.component';
import { EauthComponent } from './eauth/eauth.component';
import { PlatformAdminComponent } from './platform-admin/platform-admin.component';
import { ProdApiDocComponent } from './prod-api-doc/prod-api-doc.component';
import { ViewUserAppsComponent } from './view-user-apps/view-user-apps.component';

import { SessionStorageService } from './arch/services/session-storage.service';
import { AmexSecurityService } from './arch/services/amex-security.service';

import { ShowcaseArchWidgetsComponent } from './showcase-arch-widgets/showcase-arch-widgets.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    EssentialsComponent,
    FaqComponent,
    OnlinePrivacyComponent,
    PartnerProductApiComponent,
    ProductsApiComponent,
    AmexInputComponent,
    AmexHeaderComponent,
    AmexFooterComponent,
    AmexButtonComponent,
    AmexNavigationUtilComponent,
    CreateAppComponent,
    EauthComponent,
    PlatformAdminComponent,
    ProdApiDocComponent,
    ViewUserAppsComponent,
    ShowcaseArchWidgetsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot(),
    routing
  ],
  providers: [SessionStorageService, AmexSecurityService],
  bootstrap: [AppComponent]
})
export class AppModule { }
