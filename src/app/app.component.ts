import { Component, AfterViewInit, ViewChild, ContentChild, Input } from '@angular/core';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { Routes, RouterModule, NavigationEnd, ActivatedRoute, Router } from "@angular/router";

import { Title } from '@angular/platform-browser';
import { SessionStorageService } from './arch/services/session-storage.service';
import { AmexButtonComponent } from './arch/amex-button/amex-button.component';

@Component({
  moduleId:"appComponent",
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  providers:[SessionStorageService]
})

export class AppComponent {
  constructor(titleService:Title, router:Router, activatedRoute:ActivatedRoute) {
    router.events.subscribe(event => {
      if(event instanceof NavigationEnd) {
        var title = this.getTitle(router.routerState, router.routerState.root).join('-');
        console.log('title', title);
        titleService.setTitle(title);
      }
    });
  }

  // collect that title data properties from all child routes
  // there might be a better way but this worked for me
  getTitle(state, parent) {
    var data = [];
    if(parent && parent.snapshot.data && parent.snapshot.data.title) {
      data.push(parent.snapshot.data.title);
    }

    if(state && parent) {
      data.push(... this.getTitle(state, state.firstChild(parent)));
    }
    return data;
  }
}
