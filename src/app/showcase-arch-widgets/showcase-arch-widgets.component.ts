import { Component, AfterViewInit, ViewChild, ContentChild, Input } from '@angular/core';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { Routes,RouterModule } from "@angular/router";
import { SessionStorageService } from '../arch/services/session-storage.service';
import { AmexButtonComponent } from '../arch/amex-button/amex-button.component';

@Component({
  selector: 'app-showcase-arch-widgets',
  templateUrl: './showcase-arch-widgets.component.html',
  styleUrls: ['./showcase-arch-widgets.component.less'],
  providers:[SessionStorageService]
})
export class ShowcaseArchWidgetsComponent {
  title = 'app works!';
  btnText="button test";
  btnIconValue="dls-icon-account-filled";
  btnSize="simple";
  btnwithIcon = "simple-icon";
  btnOnlyIcon="icon";
  isDisable=true;
  
  maxValue="6";
  
  public userString:string = '';
  public user: any={
    name:"Bhavik",
    placeholder:"plac"
  }
  constructor (private sessionStorage: SessionStorageService){  }
  
  public clickEvent(){
    this.userString = JSON.stringify(this.user);
    console.info('obj = '+ this.userString);
    this.sessionStorage.setSessionObject(this.userString);
  }

  public clickEventWithParam(val){
    console.info('param = '+ val);
  }
}
