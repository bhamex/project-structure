import {Routes,RouterModule} from "@angular/router";
import {ModuleWithProviders} from "@angular/core";

import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { CreateAppComponent } from './create-app/create-app.component';
import { EauthComponent } from './eauth/eauth.component';
import { PlatformAdminComponent } from './platform-admin/platform-admin.component';
import { ProdApiDocComponent } from './prod-api-doc/prod-api-doc.component';
import { ViewUserAppsComponent } from './view-user-apps/view-user-apps.component';
import { EssentialsComponent } from './essentials/essentials.component';
import { FaqComponent } from './faq/faq.component';
import { OnlinePrivacyComponent } from './online-privacy/online-privacy.component';
import { PartnerProductApiComponent } from './product/partner-product-api/partner-product-api.component';
import { ProductsApiComponent } from './product/products-api/products-api.component';

import { ShowcaseArchWidgetsComponent } from './showcase-arch-widgets/showcase-arch-widgets.component';

const APP_ROUTE: Routes = [
    { path:'', component:HomeComponent, data: {title: 'Home Page'}},
    { path:'login', component:LoginComponent, data: {title: 'Login'}},
    { path:'create-app', component:CreateAppComponent, data: {title: 'Create App'}},
    { path:'eauth', component:EauthComponent, data: {title: 'EAuth'}},
    { path:'platform-admin', component:PlatformAdminComponent, data: {title: 'Platform Admin'}},
    { path:'prod-api-doc', component:ProdApiDocComponent, data: {title: 'Prod API Documentation'}},
    { path:'view-user-apps', component:ViewUserAppsComponent, data: {title: 'View User Apps'}},
    { path:'essentials', component:EssentialsComponent, data: {title: 'Essentials'}},
    { path:'faq', component:FaqComponent, data: {title: 'FAQ'}},
    { path:'online-privacy', component:OnlinePrivacyComponent, data: {title: 'Online Privacy'}},
    { path:'partner-product', component:PartnerProductApiComponent, data: {title: 'Partner Product'}},
    { path:'showcase-widgets', component:ShowcaseArchWidgetsComponent, data: {title: 'Showcase Widgets'}}
];

export const routing = RouterModule.forRoot(APP_ROUTE);